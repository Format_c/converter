const leftPadding = (number) => (number < 10 ? `0${number}` : `${number}`);

export const timeFormat = (date) => (`${date.getFullYear()}.${date.getMonth() + 1}.${date.getDate()} ${date.getHours()}:${leftPadding(date.getMinutes())}`);

export const numberFormat = (number) => {
  const result = number.toString().split('.');
  return `${result[0]},${result[1].substring(0,4)}`;
};
