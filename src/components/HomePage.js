import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import FormContainer from '../containers/FormContainer';
import ConvertPanel from './ConvertPanel';
import styles from '../styles/homePage.scss';

const HomePage = (props) => {
  return (
    <div className={styles.container}>
      <div className={styles.innerContainer}>
        <h1 className={styles.header}>Convert <strong>EUR</strong> to <strong>PLN</strong></h1>

        <FormContainer />
        <div className={styles.panelContainer}>
          {props.values.map((value, i) =>
            <ConvertPanel key={i} amount={value} />
          )}
        </div>
      </div>
    </div>
  );
};

HomePage.propTypes = {
  values: PropTypes.array.isRequired
};

const mapStateToProps = (state) => ({
  values: state.converter
});

export default connect(mapStateToProps)(HomePage);
