import React, { PropTypes } from 'react';
import { timeFormat, numberFormat } from '../businessLogic/formaters';
import styles from '../styles/panel.scss';

const ConvertPanel = (props) => {
  return (
    <div className={styles.panel}>
      <div className={styles.innerPanel}>
        <h3 className={styles.header}>PLN amount</h3>
        <h6 className={styles.value}>{numberFormat(props.amount.value)}</h6>
      </div>
      <div className={styles.time}>
        {timeFormat(props.amount.time)}
      </div>
    </div>
  );
};

ConvertPanel.propTypes = {
  amount: PropTypes.object.isRequired
};

export default ConvertPanel;
