import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import converterReducer from './converterReducer';

const rootReducer = combineReducers({
  routing: routerReducer,
  converter: converterReducer
});

export default rootReducer;
