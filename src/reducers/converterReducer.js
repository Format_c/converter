import { CONVERT } from '../constants/actionTypes';
// import objectAssign from 'object-assign';
import initialState from './initialState';

export default function reducer(state = initialState.converter, action) {
  switch (action.type) {
    case CONVERT:
      return state.concat({
        value: action.value * 4.2 * (1+(Math.floor(Math.random()*100) - 50)/1000),
        time: new Date
      });
    default:
      return state;
  }
}
