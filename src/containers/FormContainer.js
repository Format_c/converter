import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../actions/actions';
import styles from '../styles/form.scss';
import '../static/black-german-hat-256.png';

class FormContainer extends React.Component {
  handleSubmit(event) {
    event.preventDefault();
    this.props.actions.convert(this.refs.inputConverter.value);
  }

  render () {
    return (
      <div>
        <form onSubmit={this.handleSubmit.bind(this)} className={styles.form}>
          <img src="static/black-german-hat-256.png" className={styles.hat}/>
          <label htmlFor="inputConverter" className={styles.label}>
            Enter <strong>EUR</strong> amount
          </label>
          <br />
          <input type="text" id="inputConverter" ref="inputConverter" className={styles.input}/>
          <input type="submit" value="CONVERT TO PLN" className={styles.submit}/>
        </form>
      </div>
    );
  }
}

FormContainer.propTypes = {
  actions: PropTypes.object.isRequired
};

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(FormContainer);
