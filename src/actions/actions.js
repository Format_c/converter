import * as types from '../constants/actionTypes';

export const convert = (value) => ({
  type: types.CONVERT,
  value
});
